package com.example.andrew.pollproject;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andrew.pollproject.com.example.andrew.model.DatabaseHelper;
import com.example.andrew.pollproject.com.example.andrew.model.Poll;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Andrew on 6/28/2015.
 */
public class Polls extends Fragment {

    View myView;
    DatabaseHelper db;
    HashMap<View,Integer> pollClicks = new HashMap<View,Integer>();
    HashMap<View,View> leftRight = new HashMap<View,View>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.poll, container, false);
        db = new DatabaseHelper(getActivity().getApplicationContext());
        return myView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refresh();
    }

    public void refresh(){
        LinearLayout allPolls = (LinearLayout) myView.findViewById(R.id.allPolls);
        allPolls.removeAllViews();
        displayPolls();
    }

    public View displayPollWithInflater(Poll poll){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        String month_name = month_date.format(calendar.getTime());
        View pollView = getActivity().getLayoutInflater().inflate(R.layout.poll_template, null);
        Button left = (Button)pollView.findViewById(R.id.left_option);
        Button right = (Button)pollView.findViewById(R.id.right_option);
        TextView pollText = (TextView)pollView.findViewById(R.id.poll_text);
        TextView date = (TextView)pollView.findViewById(R.id.date);
        final ImageButton favoriteStar = (ImageButton)pollView.findViewById(R.id.favorite_star_off);

        if (poll.getCreatedAtDate().equals(month_name + " " + Calendar.DAY_OF_MONTH)){
            date.setText(poll.getCreatedAtTime());
        }
        else {
            date.setText(poll.getCreatedAtDate());
        }
        pollClicks.put(pollText, poll.getId());
        left.setText(poll.getLeft() + " (" + db.countAllAnswers(poll.getId(), DatabaseHelper.AnswerOption.LEFT) + ")");
        right.setText(poll.getRight() + " (" + db.countAllAnswers(poll.getId(), DatabaseHelper.AnswerOption.RIGHT) + ")");
        String sourceString = "<b>" + poll.getPollster() + "</b> - " + poll.getQuestion();
        pollText.setText(Html.fromHtml(sourceString));
        favoriteStar.setSelected(false);
        favoriteStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (favoriteStar.isSelected()) {
                    favoriteStar.setSelected(false);
                    favoriteStar.setColorFilter(0x00ffffff);
                } else {
                    favoriteStar.setSelected(true);
                    favoriteStar.setColorFilter(R.color.favorite_color);
                }
            }
        });
        pollText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pollIntent = new Intent(getActivity(), DetailedPoll.class);
                pollIntent.putExtra(getString(R.string.poll_id), pollClicks.get(v));
                startActivityForResult(pollIntent, 0);
            }
        });
        leftRight.put(left, right);
        leftRight.put(right, left);
        if (db.isNotAnswered(poll.getId())){
            left.setTextColor(0xff3f51b5);
            right.setTextColor(0xff3f51b5);
            left.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    db.updateAnswer(pollClicks.get(v), DatabaseHelper.AnswerOption.LEFT, 1);
                    v.setEnabled(false);
                    leftRight.get(v).setEnabled(false);
                }
            });
            right.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    db.updateAnswer(pollClicks.get(v), DatabaseHelper.AnswerOption.RIGHT, 1);
                    v.setEnabled(false);
                    leftRight.get(v).setEnabled(false);
                }
            });
        }
        else{
            left.setEnabled(false);
            right.setEnabled(false);
        }
        return pollView;
    }

    public void displayPolls(){
        LinearLayout allPolls = (LinearLayout) myView.findViewById(R.id.allPolls);
        ArrayList<Poll> polls = db.getAllPolls();
        if (polls.size() > 0) {
            for (Poll poll : polls) {
                allPolls.addView(displayPollWithInflater(poll));
            }
        }
        else{
            allPolls.addView(getActivity().getLayoutInflater().inflate(R.layout.empty_polls, null));
        }
        db.closeDB();
    }

    public void makeNewPoll(){
        if (db.getMyContacts().size() > 0) {
            Intent pollIntent = new Intent(getActivity(), SelectRecipients.class);
            pollIntent.putExtra(getString(R.string.activity), getString(R.string.new_poll));
            startActivityForResult(pollIntent, 0);
        }
        else{
            Toast.makeText(getActivity(), "Add contacts before making a poll", Toast.LENGTH_SHORT).show();
        }
        db.closeDB();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        Poll poll = new Poll(data.getStringExtra(getString(R.string.question)), data.getStringExtra(getString(R.string.left)), data.getStringExtra(getString(R.string.right)));
        poll.setId(1);
        ArrayList<Integer> personIDs = data.getIntegerArrayListExtra(getString(R.string.person_ids));
        long poll_id = db.createPoll(poll);
        personIDs.add(0,1);
        for (int person_id : personIDs) {
            db.createAnswer(person_id, poll_id);
        }
        db.closeDB();
        refresh();
    }
}
