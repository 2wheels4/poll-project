package com.example.andrew.pollproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andrew.pollproject.com.example.andrew.model.DatabaseHelper;
import com.example.andrew.pollproject.com.example.andrew.model.Group;
import com.example.andrew.pollproject.com.example.andrew.model.Person;

import java.util.ArrayList;
import java.util.HashMap;


public class NewGroup extends ActionBarActivity {

    private ArrayList<CheckBox> boxesList = new ArrayList<CheckBox>();
    private ArrayList<Integer> person_ids = new ArrayList<Integer>();
    private ArrayList<String> groupNames = new ArrayList<String>();
    private DatabaseHelper db;
    private boolean zero;
    private HashMap<View,Integer> allIDs = new HashMap<View,Integer>();
    private String newGroupName;
    private ArrayList<ArrayList<Integer>> groupLists = new ArrayList<ArrayList<Integer>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_group);
        db = new DatabaseHelper(getApplicationContext());
        displayContacts();
        getGroupLists();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_new_group) {
            createGroup();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getGroupLists(){
        ArrayList<Group> myGroups = db.getMyGroups();
        for (Group group : myGroups){
            groupNames.add(group.getName());
            groupLists.add(db.getGroupList(group.getId()));
        }
    }

    public void displayContacts(){
        LinearLayout recipientList = ((LinearLayout)findViewById(R.id.new_group__members));
        CheckBox cb;
        View contact;
        TextView name;
        for (Person person : db.getMyContacts()){
            contact = this.getLayoutInflater().inflate(R.layout.custom_contact_row, null);
            cb = (CheckBox)contact.findViewById(R.id.name_box);
            name = (TextView)contact.findViewById(R.id.name);
            name.setText(person.getName());
            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    buttonView.setChecked(isChecked);
                    if (!isChecked) {
                        //selectAll.setChecked(isChecked);
                    }
                }
            });
            contact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckBox cb = (CheckBox)v.findViewById(R.id.name_box);
                    cb.setChecked(!cb.isChecked());
                }
            });
            recipientList.addView(contact);
            allIDs.put(cb, person.getId());
            boxesList.add(cb);
        }
        db.closeDB();
    }

    public void createGroup(){
        EditText groupEditText = (EditText)findViewById(R.id.new_group_name);
        newGroupName = groupEditText.getText().toString();
        for (CheckBox cb : boxesList){
            if (cb.isChecked()){
                person_ids.add(allIDs.get(cb));
                zero = false;
            }
        }
        if(zero) {
            Toast.makeText(this, "Select members to save", Toast.LENGTH_SHORT).show();
        }
        else if (groupNames.contains(newGroupName)){
            Toast.makeText(this, "Group name already exists!", Toast.LENGTH_SHORT).show();
        }
        else if(groupLists.contains(person_ids)){
            Toast.makeText(this, "That group already exists!", Toast.LENGTH_SHORT).show();
        }
        else{
            Intent returnIntent = new Intent();
            returnIntent.putExtra(getString(R.string.new_group_name), newGroupName);
            returnIntent.putIntegerArrayListExtra(getString(R.string.person_ids), person_ids);
            setResult(RESULT_OK, returnIntent);
            finish();
        }
        person_ids.clear();
    }
}
