package com.example.andrew.pollproject.com.example.andrew.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Andrew on 6/28/2015.
 */
public class Poll {

    private String question, left, right, createdAtDate, createdAtTime, pollster;
    private int id;

    public ArrayList<Integer> getPersonIDs() {
        return personIDs;
    }

    public void setPersonIDs(ArrayList<Integer> personIDs) {
        this.personIDs = personIDs;
    }

    private ArrayList<Integer> personIDs = new ArrayList<Integer>();

    public Poll(){

    }

    public Poll(String question, String left, String right) {
        this.question = question;
        this.left = left;
        this.right = right;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        this.createdAtDate = month_date.format(calendar.getTime()) +" "+calendar.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
        this.createdAtTime = timeFormat.format(calendar.getTime());
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

    public String getCreatedAtDate() {
        return createdAtDate;
    }

    public void setCreatedAtDate(String createdAtDate) {
        this.createdAtDate = createdAtDate;
    }

    public String getCreatedAtTime() {
        return createdAtTime;
    }

    public void setCreatedAtTime(String createdAtTime) {
        this.createdAtTime = createdAtTime;
    }

    public String getPollster() {
        return pollster;
    }

    public void setPollster(String pollster) {
        this.pollster = pollster;
    }
   /* public void displayNotification(Context context){
        NotificationCompat.Builder notification = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(this.pollster)
                .setContentText(this.getQuestion())
                .addAction(0,this.getLeft(), new PendingIntent(MainActivity.class));

        NotificationManager nm =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(0, notification.build());
    }*/

}
