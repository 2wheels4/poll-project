package com.example.andrew.pollproject.com.example.andrew.model;

/**
 * Created by Andrew on 6/28/2015.
 */
public class Person{

    int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    String name;

    public Person(){

    }

    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
