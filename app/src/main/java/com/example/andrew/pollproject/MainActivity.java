package com.example.andrew.pollproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.andrew.pollproject.com.example.andrew.model.DatabaseHelper;
import com.example.andrew.pollproject.com.example.andrew.model.Poll;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private Fragment myFragment;
    public DatabaseHelper db;
    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DatabaseHelper(getApplicationContext());
        setContentView(R.layout.activity_poll);
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = "Polls";
        //setUpDatabase();
        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position)
    {
        if (position == 0)
            return;
        myFragment = null;
        mTitle = getResources().getStringArray(R.array.fragments)[position];
        switch (position) {
            case 1:
                myFragment = new Polls();
                break;
            case 2:
                myFragment = new Groups();
                break;
            case 3:
                myFragment = new FindPeople();
                break;
            case 4:
                myFragment = new Settings();
                break;
        }
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, myFragment)
                .commit();
    }

    public void onSectionAttached(int number) {
        mTitle = getResources().getStringArray(R.array.fragments)[number];
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setIcon(R.drawable.ic_drawer);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    public void setUpDatabase(){
        //set up polls
        Poll poll;
        poll = new Poll("What do you want for dinner?", "Chicken", "Salmon");
        poll.setId(2);
        poll.setCreatedAtDate("MAY 20");
        poll.setCreatedAtTime("11:20 AM");
        db.createPoll(poll);
        poll = new Poll("Are you coming tonight?", "Yes", "No");
        poll.setId(1);
        poll.setCreatedAtDate("MAY 25");
        poll.setCreatedAtTime("2:41 PM");
        db.createPoll(poll);
        poll = new Poll("When are you coming to practice tomorrow?", "7:30", "6:00");
        poll.setId(2);
        poll.setCreatedAtDate("JUN 6");
        poll.setCreatedAtTime("8:20 PM");
        db.createPoll(poll);
        poll = new Poll("What are we doing tonight?", "Going out", "Chillin'");
        poll.setId(3);
        poll.setCreatedAtDate("JUN 20");
        poll.setCreatedAtTime("6:24 PM");
        db.createPoll(poll);
        poll = new Poll("Parents just left for the weekend. Part at my house 8 pm BYOB.", "I'll be there", "Can't");
        poll.setId(6);
        poll.setCreatedAtDate("JUL 3");
        poll.setCreatedAtTime("8:30 AM");
        db.createPoll(poll);
        poll = new Poll("Are you going to the beach tomorrow?", "Yes", "No");
        poll.setId(2);
        poll.setCreatedAtDate("JUL 5");
        poll.setCreatedAtTime("11:20 AM");
        db.createPoll(poll);

        //set up contacts
        db.createPerson("Andrew");
        db.createPerson("Mike");
        db.createPerson("Ryan");
        db.createPerson("Andrew");
        db.createPerson("Tom");
        db.createPerson("Zack");
        db.createPerson("Schwartz");
        db.createPerson("Rosenheck");
        db.createPerson("Scott");
        db.createPerson("Josh");
        db.createPerson("Alex");
        db.createPerson("Schlissel");
        db.createPerson("Jordyn");

        //set up answers
        db.createAnswer(1, 1);
        db.updateAnswer(1, DatabaseHelper.AnswerOption.LEFT, 1);
        db.createAnswer(3, 1);
        db.createAnswer(4, 1);
        db.updateAnswer(1, DatabaseHelper.AnswerOption.LEFT, 4);
        db.createAnswer(6, 1);
        db.createAnswer(10, 1);
        db.updateAnswer(1, DatabaseHelper.AnswerOption.RIGHT, 10);
        db.createAnswer(1, 2);
        db.createAnswer(3, 2);
        db.updateAnswer(2, DatabaseHelper.AnswerOption.RIGHT, 3);
        db.createAnswer(5, 2);
        db.updateAnswer(2, DatabaseHelper.AnswerOption.LEFT, 5);
        db.createAnswer(6, 2);
        db.updateAnswer(2, DatabaseHelper.AnswerOption.RIGHT, 6);
        db.createAnswer(7, 2);
        db.updateAnswer(2, DatabaseHelper.AnswerOption.RIGHT, 7);
        db.createAnswer(8, 2);
        db.createAnswer(9, 2);
        db.createAnswer(10, 2);
        db.updateAnswer(2, DatabaseHelper.AnswerOption.RIGHT, 10);
        db.createAnswer(13, 2);
        db.createAnswer(1, 3);
        db.updateAnswer(3, DatabaseHelper.AnswerOption.RIGHT, 10);
        db.createAnswer(1, 3);
        db.updateAnswer(3, DatabaseHelper.AnswerOption.RIGHT, 1);
        db.createAnswer(4, 3);
        db.updateAnswer(3, DatabaseHelper.AnswerOption.RIGHT, 4);
        db.createAnswer(5, 3);
        db.updateAnswer(3, DatabaseHelper.AnswerOption.RIGHT, 5);
        db.createAnswer(8, 3);
        db.updateAnswer(3, DatabaseHelper.AnswerOption.RIGHT, 8);
        db.createAnswer(11, 3);
        db.updateAnswer(3, DatabaseHelper.AnswerOption.LEFT, 11);
        db.createAnswer(1, 4);
        db.createAnswer(2, 4);
        db.updateAnswer(4, DatabaseHelper.AnswerOption.RIGHT, 10);
        db.createAnswer(7, 4);
        db.createAnswer(1, 5);
        db.createAnswer(6, 5);
        db.updateAnswer(5, DatabaseHelper.AnswerOption.RIGHT, 6);
        db.createAnswer(8, 5);
        db.updateAnswer(5, DatabaseHelper.AnswerOption.RIGHT, 8);
        db.createAnswer(9, 5);
        db.createAnswer(1, 6);
        db.updateAnswer(6, DatabaseHelper.AnswerOption.RIGHT, 1);
        db.createAnswer(4, 6);
        db.updateAnswer(6, DatabaseHelper.AnswerOption.LEFT, 4);
        db.createAnswer(5, 6);
        db.updateAnswer(6, DatabaseHelper.AnswerOption.RIGHT, 5);
        db.createAnswer(7, 6);
        db.createAnswer(9, 6);
        db.updateAnswer(6, DatabaseHelper.AnswerOption.LEFT, 9);
        db.createAnswer(10, 6);
        db.updateAnswer(6, DatabaseHelper.AnswerOption.RIGHT, 10);
        db.createAnswer(11, 6);
        db.closeDB();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            if (myFragment instanceof Settings){
                getMenuInflater().inflate(R.menu.settings, menu);
            }
            else if (myFragment instanceof Groups){
                getMenuInflater().inflate(R.menu.group, menu);
            }
            else{
                getMenuInflater().inflate(R.menu.poll, menu);
            }
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {
            if (myFragment instanceof Polls){
                ((Polls) myFragment).makeNewPoll();
            }
            else{
                ((Groups) myFragment).makeNewGroup();
            }
            return true;
        }
        if (id == R.id.action_save) {
            if (myFragment instanceof Settings){
                ((Settings) myFragment).save();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == RESULT_OK){
            super.onActivityResult(requestCode,  resultCode, data);
        }
    }

}
