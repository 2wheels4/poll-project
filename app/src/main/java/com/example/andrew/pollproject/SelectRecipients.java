package com.example.andrew.pollproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andrew.pollproject.com.example.andrew.model.DatabaseHelper;
import com.example.andrew.pollproject.com.example.andrew.model.Person;

import java.util.ArrayList;
import java.util.HashMap;


public class SelectRecipients extends ActionBarActivity {

    private ArrayList<CheckBox> boxesList = new ArrayList<CheckBox>();
    private ArrayList<Integer> person_ids = new ArrayList<Integer>();
    private DatabaseHelper db;
    private boolean zero;
    private HashMap<View,Integer> allIDs = new HashMap<View,Integer>();
    //private CheckBox selectAll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_recipients);
        db = new DatabaseHelper(getApplicationContext());
        displayContacts();
        /*selectAll = (CheckBox) findViewById(R.id.selectAll);
        selectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (CheckBox cb : boxesList){
                    cb.setChecked(((CheckBox)v).isChecked());
                }
            }
        });*/
        zero = true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_select_recipients, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_next) {
            next();
        }
        return super.onOptionsItemSelected(item);
    }

    public void displayContacts(){
        LinearLayout recipientList = ((LinearLayout)findViewById(R.id.contacts));
        CheckBox cb;
        View contact;
        TextView name;
        for (Person person : db.getMyContacts()){
            contact = this.getLayoutInflater().inflate(R.layout.custom_contact_row, null);
            cb = (CheckBox)contact.findViewById(R.id.name_box);
            name = (TextView)contact.findViewById(R.id.name);
            name.setText(person.getName());
            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    buttonView.setChecked(isChecked);
                    if (!isChecked) {
                        //selectAll.setChecked(isChecked);
                    }
                }
            });
            contact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckBox cb = (CheckBox)v.findViewById(R.id.name_box);
                    cb.setChecked(!cb.isChecked());
                }
            });
            recipientList.addView(contact);
            allIDs.put(cb, person.getId());
            boxesList.add(cb);
        }
        db.closeDB();
    }

    public void next(){
        for (CheckBox cb : boxesList){
            if (cb.isChecked()){
                person_ids.add(allIDs.get(cb));
                zero = false;
            }
        }
        if(zero) {
            Toast.makeText(this, "Select recipients to continue", Toast.LENGTH_SHORT).show();
        }
        else{
            Intent makePoll = new Intent(this, MakePoll.class);
            startActivityForResult(makePoll, 0);
        }
    }

    public void cancel(View view){
        Intent returnIntent = new Intent();
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == RESULT_OK){
            data.putIntegerArrayListExtra(getString(R.string.person_ids), person_ids);
            data.putExtra(getString(R.string.activity), getString(R.string.new_poll));
            setResult(RESULT_OK, data);
            finish();
        }
        else{
            getIntent().putExtra(getString(R.string.activity), getString(R.string.new_poll));
            /*
            Intent returnIntent = new Intent();
            setResult(RESULT_CANCELED,returnIntent);
            finish();*/
        }
    }
}
