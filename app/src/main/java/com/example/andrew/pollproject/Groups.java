package com.example.andrew.pollproject;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andrew.pollproject.com.example.andrew.model.DatabaseHelper;
import com.example.andrew.pollproject.com.example.andrew.model.Group;

import java.util.ArrayList;

/**
 * Created by Andrew on 6/28/2015.
 */
public class Groups extends Fragment {

    ArrayList<String> groupNames = new ArrayList<String>();
    DatabaseHelper db;
    View myView;
    String newGroupName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        db = new DatabaseHelper(getActivity().getApplicationContext());
        myView = inflater.inflate(R.layout.groups, container, false);
        return myView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refresh();
    }

    public void refresh(){
        LinearLayout ll = (LinearLayout)myView.findViewById(R.id.group_container);
        ll.removeAllViews();
        displayGroups();
    }

    public void displayGroups() {
        LinearLayout ll = (LinearLayout) myView.findViewById(R.id.group_container);
        View groupRow;
        ArrayList<Group> myGroups = db.getMyGroups();
        if (myGroups.size() > 0){
            TextView groupName;
            for (Group group : myGroups) {
                groupRow = getActivity().getLayoutInflater().inflate(R.layout.custom_group_row, null);
                groupName = (TextView) groupRow.findViewById(R.id.group_name);
                groupName.setText(group.getName());
                groupNames.add(group.getName());
                ll.addView(groupRow);
            }
        }
        else{
            groupRow = getActivity().getLayoutInflater().inflate(R.layout.empty_groups, null);
            ll.addView(groupRow);
        }
        db.closeDB();
    }

    public void makeNewGroup(){
        if (db.getMyContacts().size() > 0) {
            Intent intent = new Intent(getActivity(), NewGroup.class);
            intent.putStringArrayListExtra(getString(R.string.my_group_names), groupNames);
            startActivityForResult(intent, 0);
        }
        else{
            Toast.makeText(getActivity(), "Add contacts before making a group", Toast.LENGTH_SHORT).show();
        }
        db.closeDB();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == getActivity().RESULT_OK){
            ArrayList<Integer> personIDs = data.getIntegerArrayListExtra(getString(R.string.person_ids));
            groupNames.add(data.getStringExtra(getString(R.string.new_group_name)));
            long group_id = db.createGroup(newGroupName);
            for (int person_id : personIDs){
                db.createGroupEntry((int)group_id, person_id);
            }
            db.closeDB();
        }
        refresh();
    }
}
