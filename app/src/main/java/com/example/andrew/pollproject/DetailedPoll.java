package com.example.andrew.pollproject;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.andrew.pollproject.com.example.andrew.model.DatabaseHelper;
import com.example.andrew.pollproject.com.example.andrew.model.Poll;


public class DetailedPoll extends ActionBarActivity {

    DatabaseHelper db;
    Poll poll;
    AlertDialog dialog;
    CheckBox dontAskAgain;
    Button confirm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_poll);
        db = new DatabaseHelper(getApplicationContext());
        poll = db.getPoll(getIntent().getIntExtra(getString(R.string.poll_id), 0));
        displayDetailedPoll();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detailed_poll, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_run_again) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            if (prefs.getBoolean(getString(R.string.runDialogs), true)) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
                dialogBuilder.setView(getLayoutInflater().inflate(R.layout.confirm_run_again, null));
                dialog = dialogBuilder.create();
                dialog.show();
                dontAskAgain = (CheckBox)dialog.findViewById(R.id.dont_ask_run);
                confirm = (Button)dialog.findViewById(R.id.confirm_run);
            }
            else{
                runAgain();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void runAgain(){
        Intent returnIntent = new Intent();
        returnIntent.putExtra(getString(R.string.question), poll.getQuestion());
        returnIntent.putExtra(getString(R.string.left), poll.getLeft());
        returnIntent.putExtra(getString(R.string.right), poll.getRight());
        returnIntent.putIntegerArrayListExtra(getString(R.string.person_ids), poll.getPersonIDs());
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    public void displayDetailedPoll(){
        TextView question = (TextView)findViewById(R.id.detailed_question);
        TextView left = (TextView)findViewById(R.id.leftAnswer);
        TextView right = (TextView)findViewById(R.id.rightAnswer);
        TextView pending = (TextView)findViewById(R.id.pending);
        TextView pollster = (TextView)findViewById(R.id.pollster);
        TextView dateTime = (TextView)findViewById(R.id.date_time);
        LinearLayout leftPeople = (LinearLayout)findViewById(R.id.leftResponses);
        LinearLayout rightPeople = (LinearLayout)findViewById(R.id.rightResponses);
        LinearLayout pendingPeople = (LinearLayout)findViewById(R.id.pendingResponses);
        TextView name;

        question.setText(poll.getQuestion());
        pollster.setText("Created by "+poll.getPollster());
        dateTime.setText("   "+poll.getCreatedAtDate() + ", " + poll.getCreatedAtTime()+"   ");
        left.setText(poll.getLeft() + " (" + db.countAllAnswers(poll.getId(), DatabaseHelper.AnswerOption.LEFT) + ")");
        left.setGravity(Gravity.CENTER);
        right.setText(poll.getRight() + " (" + db.countAllAnswers(poll.getId(), DatabaseHelper.AnswerOption.RIGHT) + ")");
        right.setGravity(Gravity.CENTER);
        pending.setText("Pending Responses (" + db.countAllAnswers(poll.getId(), DatabaseHelper.AnswerOption.PENDING) + ")");
        pending.setGravity(Gravity.CENTER);

        displayHelper(leftPeople, DatabaseHelper.AnswerOption.LEFT);
        displayHelper(rightPeople, DatabaseHelper.AnswerOption.RIGHT);
        displayHelper(pendingPeople, DatabaseHelper.AnswerOption.PENDING);

        db.closeDB();
    }

    public void dontAsk(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(getString(R.string.runDialogs), !dontAskAgain.isChecked());
        editor.apply();
        dialog.dismiss();
    }

    public void dialogResult(View view){
        dontAsk();
        if(view == confirm){
            runAgain();
        }
    }

    public void displayHelper(LinearLayout linearLayout, DatabaseHelper.AnswerOption answer){
        View row;
        TextView name;
        for (String person : db.getAllAnswers(poll.getId(),answer)){
            if (answer.getAnswer() == DatabaseHelper.AnswerOption.LEFT.getAnswer()) {
                row = this.getLayoutInflater().inflate(R.layout.custom_response_row_left, null);
                name = (TextView) row.findViewById(R.id.response_name_left);
            }
            else if (answer.getAnswer() == DatabaseHelper.AnswerOption.RIGHT.getAnswer()) {
                row = this.getLayoutInflater().inflate(R.layout.custom_response_row_right, null);
                name = (TextView) row.findViewById(R.id.response_name_right);
            }
            else{
                row = this.getLayoutInflater().inflate(R.layout.custom_response_row_pend, null);
                name = (TextView) row.findViewById(R.id.response_name_pend);
            }
            name.setText(person);
            linearLayout.addView(row);
        }
    }
}
