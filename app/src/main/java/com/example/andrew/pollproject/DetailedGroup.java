package com.example.andrew.pollproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.andrew.pollproject.com.example.andrew.model.DatabaseHelper;
import com.example.andrew.pollproject.com.example.andrew.model.Poll;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


public class DetailedGroup extends ActionBarActivity {

    RelativeLayout.LayoutParams paramsLeft = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    RelativeLayout.LayoutParams paramsRight = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    View myView;
    DatabaseHelper db;
    HashMap<View,Integer> pollClicks = new HashMap<View,Integer>();
    HashMap<View,View> leftRight = new HashMap<View,View>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_group);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detailed_group, menu);
        getActionBar().setTitle(getIntent().getStringExtra(getString(R.string.group_name)));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public TableRow buildRowOne(Poll poll){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        String month_name = month_date.format(calendar.getTime());

        TableRow tableRow = new TableRow(this);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        TextView question = new TextView(this);
        TextView date = new TextView(this);
        question.setText(poll.getQuestion());
        question.setTextSize(14);
        if (poll.getCreatedAtDate().equals(month_name + " " + Calendar.DAY_OF_MONTH)){
            date.setText(poll.getCreatedAtTime());
        }
        else {
            date.setText(poll.getCreatedAtDate());
        }
        date.setTextSize(8);
        relativeLayout.addView(question, paramsLeft);
        relativeLayout.addView(date, paramsRight);
        tableRow.addView(relativeLayout);
        pollClicks.put(tableRow, poll.getId());
        tableRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pollIntent = new Intent(getApplicationContext(), DetailedPoll.class);
                pollIntent.putExtra(getString(R.string.poll_id), pollClicks.get(v));
                pollIntent.putExtra(getString(R.string.activity), getString(R.string.group_poll));
                startActivityForResult(pollIntent, 0);
            }
        });
        return tableRow;
    }

    public TableRow buildRowTwo(Poll poll){
        TableRow tableRow = new TableRow(this);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        TextView pollster = new TextView(this);
        pollster.setText(poll.getPollster());
        pollster.setTextSize(8);
        ImageButton ib = new ImageButton(this);
        ib.setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
        ib.setSelected(false);
        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.isSelected()) {
                    ((ImageButton) v).setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                    v.setSelected(false);
                } else {
                    ((ImageButton) v).setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
                    v.setSelected(true);
                }
            }
        });

        relativeLayout.addView(pollster, paramsLeft);
        relativeLayout.addView(ib, paramsRight);
        tableRow.addView(relativeLayout);

        return tableRow;
    }

    public TableRow buildRowThree(Poll poll){
        TableRow tableRow = new TableRow(this);
        Button left = new Button(this);
        Button right = new Button(this);
        left.setText(poll.getLeft()+db.countAllAnswers(poll.getId(), DatabaseHelper.AnswerOption.LEFT));
        right.setText(poll.getRight()+db.countAllAnswers(poll.getId(), DatabaseHelper.AnswerOption.RIGHT));
        left.setTextSize(10);
        right.setTextSize(10);
        left.setGravity(Gravity.CENTER);
        right.setGravity(Gravity.CENTER);
        leftRight.put(left, right);
        leftRight.put(right, left);
        if (db.isNotAnswered(poll.getId())){
            left.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    db.updateAnswer(pollClicks.get(v), DatabaseHelper.AnswerOption.LEFT, 1);
                    v.setEnabled(false);
                    leftRight.get(v).setEnabled(false);
                }
            });
            right.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    db.updateAnswer(pollClicks.get(v), DatabaseHelper.AnswerOption.RIGHT, 1);
                    v.setEnabled(false);
                    leftRight.get(v).setEnabled(false);
                }
            });
        }
        else{
            left.setEnabled(false);
            right.setEnabled(false);
        }
        tableRow.addView(left);
        tableRow.addView(right);

        return tableRow;
    }

    public void displayPolls(){
        LinearLayout allPolls = (LinearLayout)findViewById(R.id.groupPolls);
        ArrayList<Poll> polls = db.getGroupPoll(getIntent().getIntExtra(getString(R.string.group_id), 0));
        for (Poll poll : polls) {
            allPolls.addView(buildRowOne(poll));
            allPolls.addView(buildRowTwo(poll));
            allPolls.addView(buildRowThree(poll));
        }
        db.closeDB();
    }

    public void makeNewPoll(){
        Intent pollIntent = new Intent(this, SelectRecipients.class);
        startActivityForResult(pollIntent, 0);
    }


}
