package com.example.andrew.pollproject.com.example.andrew.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by Andrew on 7/1/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    // Logcat tag
    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "simpoll";

    public enum AnswerOption{
        LEFT(0), RIGHT(1), PENDING(2);

        private int answer;

        AnswerOption(int answer){
            this.answer = answer;
        }

        public int getAnswer(){
            return this.answer;
        }
    }

    // Table Names
    private static final String TABLE_POLLS = "polls2";
    private static final String TABLE_PERSONS = "persons2";
    private static final String TABLE_ANSWERS = "answers2";
    private static final String TABLE_GROUPS = "groups2";
    private static final String TABLE_GROUP_ENTRIES = "group_entries2";
    private static final String TABLE_GROUP_POLLS = "group_polls2";

    // Common column names
    private static final String KEY_ID = "id";
    private static final String KEY_PERSON_ID = "person_id";
    private static final String KEY_NAME = "name";

    // Polls Table - column names
    public static final String KEY_QUESTION = "question";
    public static final String KEY_LEFT = "left";
    public static final String KEY_RIGHT = "right";
    public static final String KEY_CREATED_AT_DATE = "created_date";
    public static final String KEY_CREATED_AT_TIME = "created_time";

    // Answers Table - column names
    private static final String KEY_ANSWER = "answer";
    private static final String KEY_POLL_ID = "poll_id";

    // Person Table - column names

    // Groups Table - column names

    // GroupEntries Table - column names
    private static final String KEY_GROUP_ID = "group_id";

    //Group Poll Table

    // Table Create Statements
    // Poll table create statement
    private static final String CREATE_TABLE_POLLS = "CREATE TABLE "
            + TABLE_POLLS + "(" + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_QUESTION
            + " TEXT, " + KEY_LEFT + " TEXT, "+ KEY_RIGHT + " TEXT," +
            KEY_CREATED_AT_DATE + " TEXT, " + KEY_CREATED_AT_TIME
            + " TEXT, " + KEY_PERSON_ID + " INTEGER" + ")";

    // Answer table create statement
    private static final String CREATE_TABLE_ANSWERS = "CREATE TABLE " + TABLE_ANSWERS
            + "(" + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_ANSWER + " INTEGER, "
            + KEY_PERSON_ID + " INTEGER, " + KEY_POLL_ID + " INTEGER" + ")";

    // Person table create statement
    private static final String CREATE_TABLE_PERSONS = "CREATE TABLE "
            + TABLE_PERSONS + "(" + KEY_ID + " INTEGER PRIMARY KEY, "
            + KEY_NAME + " TEXT" + ")";

    // Group table create statement
    private static final String CREATE_TABLE_GROUPS = "CREATE TABLE "
            + TABLE_GROUPS + "(" + KEY_ID + " INTEGER PRIMARY KEY, "
            + KEY_NAME + " TEXT" + ")";

    // GroupEntries table create statement
    private static final String CREATE_TABLE_GROUP_ENTRIES = "CREATE TABLE "
            + TABLE_GROUP_ENTRIES + "(" + KEY_ID + " INTEGER PRIMARY KEY, "
            + KEY_GROUP_ID + " INTEGER, "  + KEY_PERSON_ID + " INTEGER" + ")";

    //GroupPoll table create statement
    private static final String CREATE_TABLE_GROUP_POLLS = "CREATE TABLE "
            + TABLE_GROUP_POLLS + "(" + KEY_ID + " INTEGER PRIMARY KEY, "
            + KEY_GROUP_ID + " INTEGER, "  + KEY_POLL_ID + " INTEGER" + ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // creating required tables
        db.execSQL(CREATE_TABLE_POLLS);
        db.execSQL(CREATE_TABLE_ANSWERS);
        db.execSQL(CREATE_TABLE_PERSONS);
        db.execSQL(CREATE_TABLE_GROUPS);
        db.execSQL(CREATE_TABLE_GROUP_ENTRIES);
        db.execSQL(CREATE_TABLE_GROUP_POLLS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_POLLS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ANSWERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PERSONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUPS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUP_ENTRIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUP_POLLS);

        // create new tables
        onCreate(db);
    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    public ArrayList<Poll> getAllPolls(){
        ArrayList<Poll> polls = new ArrayList<Poll>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+TABLE_POLLS+ " LEFT JOIN "+TABLE_PERSONS+" ON "+TABLE_POLLS+"."+KEY_PERSON_ID + "=" + TABLE_PERSONS+"."+KEY_ID;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToLast()) {
            do {
                Poll poll = new Poll();
                poll.setId(c.getInt(0));
                poll.setQuestion(c.getString(c.getColumnIndex(KEY_QUESTION)));
                poll.setLeft(c.getString(c.getColumnIndex(KEY_LEFT)));
                poll.setRight(c.getString(c.getColumnIndex(KEY_RIGHT)));
                poll.setCreatedAtDate(c.getString(c.getColumnIndex(KEY_CREATED_AT_DATE)));
                poll.setCreatedAtTime(c.getString(c.getColumnIndex(KEY_CREATED_AT_TIME)));
                poll.setPollster(c.getString(c.getColumnIndex(KEY_NAME)));
                polls.add(poll);
            } while (c.moveToPrevious());
        }
        c.close();
        return polls;
    }

    public Poll getPoll(int poll_id){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+TABLE_POLLS+ " LEFT JOIN "+TABLE_PERSONS+" ON "+TABLE_POLLS+"."+KEY_PERSON_ID+"="+TABLE_PERSONS+"."+KEY_ID+" WHERE "+TABLE_POLLS+"."+KEY_ID+"= '"+poll_id+"'";
        Cursor c = db.rawQuery(selectQuery, null);
        Poll poll = new Poll();
        if (c.moveToLast()) {
            do {
                poll.setId(c.getInt((c.getColumnIndex(KEY_ID))));
                poll.setQuestion(c.getString(c.getColumnIndex(KEY_QUESTION)));
                poll.setLeft(c.getString(c.getColumnIndex(KEY_LEFT)));
                poll.setRight(c.getString(c.getColumnIndex(KEY_RIGHT)));
                poll.setCreatedAtDate(c.getString(c.getColumnIndex(KEY_CREATED_AT_DATE)));
                poll.setCreatedAtTime(c.getString(c.getColumnIndex(KEY_CREATED_AT_TIME)));
                poll.setPollster(c.getString(c.getColumnIndex(KEY_NAME)));
            } while (c.moveToPrevious());
        }
        c.close();
        return poll;
    }

    public ArrayList<Poll> getMyPolls(){
        ArrayList<Poll> polls = new ArrayList<Poll>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+TABLE_POLLS+ " LEFT JOIN "+TABLE_PERSONS+" ON "+TABLE_POLLS+"."+KEY_PERSON_ID+"="+TABLE_PERSONS+"."+KEY_PERSON_ID+" WHERE "+KEY_PERSON_ID+"='1'";
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToLast()) {
            do {
                Poll poll = new Poll();
                poll.setId(c.getInt((c.getColumnIndex(KEY_ID))));
                poll.setQuestion(c.getString(c.getColumnIndex(KEY_QUESTION)));
                poll.setLeft(c.getString(c.getColumnIndex(KEY_LEFT)));
                poll.setRight(c.getString(c.getColumnIndex(KEY_RIGHT)));
                poll.setCreatedAtDate(c.getString(c.getColumnIndex(KEY_CREATED_AT_DATE)));
                poll.setCreatedAtTime(c.getString(c.getColumnIndex(KEY_CREATED_AT_TIME)));
                poll.setPollster(c.getString(c.getColumnIndex(KEY_NAME)));
                polls.add(poll);
            } while (c.moveToPrevious());
        }
        c.close();
        return polls;
    }

    public long createPoll(Poll poll){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_QUESTION, poll.getQuestion());
        values.put(KEY_CREATED_AT_DATE, poll.getCreatedAtDate());
        values.put(KEY_CREATED_AT_TIME, poll.getCreatedAtTime());
        values.put(KEY_LEFT, poll.getLeft());
        values.put(KEY_RIGHT, poll.getRight());
        values.put(KEY_PERSON_ID, poll.getId());
        //add person id
        long poll_id = db.insert(TABLE_POLLS, null, values);

        return poll_id;
    }

    public long createPerson(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_NAME, name);

        long person_id = db.insert(TABLE_PERSONS, null, values);

        return person_id;
    }

    public long createAnswer(long person_id, long poll_id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_PERSON_ID, person_id);
        values.put(KEY_POLL_ID, poll_id);
        values.put(KEY_ANSWER, AnswerOption.PENDING.getAnswer());

        long answer_id = db.insert(TABLE_ANSWERS, null, values);

        return answer_id;
    }

    public long createGroup(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_NAME, name);

        long group_id = db.insert(TABLE_GROUPS, null, values);

        return group_id;
    }

    public long createGroupPoll(int poll_id, int group_id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_GROUP_ID, group_id);
        values.put(KEY_POLL_ID, poll_id);

        long group_poll_id = db.insert(TABLE_GROUP_POLLS, null, values);

        return group_poll_id;
    }

    public long createGroupEntry(int group_id, int person_id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_GROUP_ID, group_id);
        values.put(KEY_PERSON_ID, person_id);

        long entry_id = db.insert(TABLE_GROUP_ENTRIES, null, values);

        return entry_id;
    }

    public ArrayList<String> getAllAnswers(int poll_id, AnswerOption answer){
        ArrayList<String> answers = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT "+TABLE_PERSONS+"."+KEY_NAME+" FROM "+TABLE_PERSONS+" INNER JOIN "+TABLE_ANSWERS+" " +
                "ON "+TABLE_PERSONS+"."+KEY_ID+" = "+TABLE_ANSWERS+"."+KEY_PERSON_ID+" WHERE "+KEY_POLL_ID+"" +
                " = '"+poll_id+"' AND "+KEY_ANSWER + " = '" + answer.getAnswer()+"'";
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToLast()) {
            do {
                answers.add(c.getString(c.getColumnIndex(KEY_NAME)));
            } while (c.moveToPrevious());
        }
        c.close();
        return answers;
    }

    public int countAllAnswers(int poll_id, AnswerOption answer){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+TABLE_ANSWERS+" WHERE "+KEY_POLL_ID+" = '"+poll_id+"' AND "+KEY_ANSWER+" = '"+answer.getAnswer()+"'";
        Cursor c = db.rawQuery(selectQuery, null);
        int count = c.getCount();
        return count;
    }

    public ArrayList<Group> getMyGroups(){
        ArrayList<Group> myGroups = new ArrayList<Group>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+TABLE_GROUPS;
        Group group;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToLast()) {
            do {
                group = new Group();
                group.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                group.setName(c.getString(c.getColumnIndex(KEY_NAME)));
                myGroups.add(group);
            } while (c.moveToPrevious());
        }
        c.close();
        return myGroups;
    }

    public ArrayList<Person> getMyContacts(){
        ArrayList<Person> myContacts = new ArrayList<Person>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+TABLE_PERSONS+" WHERE "+KEY_ID+" != '1'";
        Person person;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToLast()) {
            do {
                person = new Person();
                person.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                person.setName(c.getString(c.getColumnIndex(KEY_NAME)));
                myContacts.add(person);
            } while (c.moveToPrevious());
        }
        c.close();
        return myContacts;
    }

    public ArrayList<String> getMyStrings(String key){
        ArrayList<String> myStrings = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT "+key+" FROM "+TABLE_POLLS+" WHERE "+KEY_PERSON_ID+" = '1'";
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToLast()) {
            do {
                if (!myStrings.contains(c.getString(c.getColumnIndex(key))))
                    myStrings.add(c.getString(c.getColumnIndex(key)));
            } while (c.moveToPrevious());
        }
        c.close();
        return myStrings;
    }

    public boolean isNotAnswered(int poll_id){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT "+KEY_ANSWER+" FROM "+TABLE_ANSWERS+" WHERE "+KEY_POLL_ID+" = '"+poll_id+"' AND "+KEY_PERSON_ID+" = '1'";
        Cursor c = db.rawQuery(selectQuery, null);
        boolean notAnswered;
        /*if (c!= null)
            notAnswered = c.getInt(c.getColumnIndex(KEY_ANSWER)) == AnswerOption.PENDING.getAnswer();
        else
            notAnswered = false;
           */
        c.close();
        return false;
    }

    public void updateAnswer(int poll_id, AnswerOption answer, int person_id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(KEY_ANSWER, answer.getAnswer());
        db.update(TABLE_ANSWERS, args, KEY_POLL_ID + "=" + poll_id + " AND " + KEY_PERSON_ID + "=" + person_id, null);
    }

    public void updateGroupName(int group_id, String name){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(KEY_NAME, name);
        db.update(TABLE_GROUPS, args, KEY_ID + "=" + group_id, null);
    }

    public String getMyName(){
        SQLiteDatabase db = this.getReadableDatabase();
        String myName = "";
        String selectQuery = "SELECT * FROM "+TABLE_PERSONS+" WHERE "+KEY_ID+" != '1'";
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
                myName = c.getString(c.getColumnIndex(KEY_NAME));
        }
        c.close();
        return myName;
    }

    public void updateMyName(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(KEY_NAME, name);
        db.update(TABLE_PERSONS, args, KEY_ID + "=" + 1, null);
    }

    public ArrayList<Poll> getGroupPoll(int group_id){
        ArrayList<Poll> polls = new ArrayList<Poll>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+TABLE_GROUP_POLLS+ "LEFT JOIN "+TABLE_POLLS+" ON "+TABLE_POLLS+"."+KEY_ID + "=" + TABLE_GROUP_POLLS+"."+KEY_POLL_ID+" WHERE "+TABLE_GROUP_POLLS+"."+KEY_GROUP_ID+" = '"+group_id+"'";
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToLast()) {
            do {
                Poll poll = new Poll();
                poll.setId(c.getInt((c.getColumnIndex(KEY_ID))));
                poll.setQuestion(c.getString(c.getColumnIndex(KEY_QUESTION)));
                poll.setLeft(c.getString(c.getColumnIndex(KEY_LEFT)));
                poll.setRight(c.getString(c.getColumnIndex(KEY_RIGHT)));
                poll.setCreatedAtDate(c.getString(c.getColumnIndex(KEY_CREATED_AT_DATE)));
                poll.setCreatedAtTime(c.getString(c.getColumnIndex(KEY_CREATED_AT_TIME)));
                poll.setPollster(c.getString(c.getColumnIndex(KEY_NAME)));
                polls.add(poll);
            } while (c.moveToPrevious());
        }
        c.close();
        return polls;
    }

    public void deleteEverything(){
        SQLiteDatabase dbw = this.getWritableDatabase();
        for (int x = 0; x < 100; x++) {
            dbw.delete(TABLE_PERSONS, KEY_ID + "='1'", null);
            dbw.delete(TABLE_GROUPS, KEY_ID + "='1'", null);
            dbw.delete(TABLE_ANSWERS, KEY_ID + "='1'", null);
        }
    }

    public String getGroupDate(int group_id){
        return "JUL 11";
    }

    public ArrayList<Integer> getGroupList(int group_id){
        ArrayList<Integer> myMembers = new ArrayList<Integer>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+TABLE_GROUP_ENTRIES +" WHERE " + KEY_GROUP_ID + " = " + group_id;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToLast()) {
            do {
                myMembers.add(c.getInt(c.getColumnIndex(KEY_PERSON_ID)));
            } while (c.moveToPrevious());
        }
        c.close();
        return myMembers;
    }
}