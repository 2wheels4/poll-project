package com.example.andrew.pollproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.andrew.pollproject.com.example.andrew.model.DatabaseHelper;


public class MakePoll extends ActionBarActivity {

    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_poll);
        db = new DatabaseHelper(getApplicationContext());
        displaySavedOptions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_make_poll, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_send) {
            send();
        }

        return super.onOptionsItemSelected(item);
    }

    public void displaySavedOptions(){
        LinearLayout questionLayout = (LinearLayout)findViewById(R.id.questionOptions);
        LinearLayout leftLayout = (LinearLayout)findViewById(R.id.leftOptions);
        LinearLayout rightLayout = (LinearLayout)findViewById(R.id.rightOptions);

        displayHelper(questionLayout, DatabaseHelper.KEY_QUESTION, R.id.question, Gravity.LEFT);
        displayHelper(leftLayout, DatabaseHelper.KEY_LEFT, R.id.leftText, Gravity.LEFT);
        displayHelper(rightLayout, DatabaseHelper.KEY_RIGHT, R.id.rightText, Gravity.RIGHT);

        db.closeDB();
    }

    public void displayHelper(LinearLayout ll, String key, final int editTextId, int gravity){
        TextView tv;
        for (String string : db.getMyStrings(key)){
            tv = new TextView(this);
            tv.setText(string);
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText option = (EditText) findViewById(editTextId);
                    option.setText(((TextView) v).getText());
                }
            });
            tv.setGravity(gravity);
            tv.setTextSize(20);
            ll.addView(tv);
        }
    }

    public void send(){
        Intent returnIntent = new Intent();
        EditText question = (EditText)findViewById(R.id.question);
        EditText leftOption = (EditText)findViewById(R.id.leftText);
        EditText rightOption = (EditText)findViewById(R.id.rightText);
        returnIntent.putExtra(getString(R.string.question), question.getText().toString());
        returnIntent.putExtra(getString(R.string.left), leftOption.getText().toString());
        returnIntent.putExtra(getString(R.string.right), rightOption.getText().toString());
        setResult(RESULT_OK,returnIntent);
        finish();
    }

    public void cancel(View view){
        Intent returnIntent = new Intent();
        setResult(RESULT_CANCELED,returnIntent);
        finish();
    }
}
