package com.example.andrew.pollproject;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.example.andrew.pollproject.com.example.andrew.model.DatabaseHelper;

/**
 * Created by Andrew on 6/28/2015.
 */
public class Settings extends Fragment {

    DatabaseHelper db;
    View myView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.settings, container, false);
        db = new DatabaseHelper(getActivity().getApplicationContext());
        displaySettings(container);
        return myView;
    }

    public void displaySettings(ViewGroup container){
        EditText userName = (EditText)myView.findViewById(R.id.username);
        userName.setText(db.getMyName());
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        CheckBox runDialogs = (CheckBox)myView.findViewById(R.id.runAgainCB);
        CheckBox removeDialogs = (CheckBox)myView.findViewById(R.id.removeCB);
        runDialogs.setChecked(prefs.getBoolean(getString(R.string.runDialogs), true));
        removeDialogs.setChecked(prefs.getBoolean(getString(R.string.removeDialogs),true));
        displayHelper(removeDialogs.isChecked(), R.id.removeText, "Remove confirmations");
        displayHelper(runDialogs.isChecked(), R.id.runAgainText, "Run again confirmations");
        runDialogs.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean(getString(R.string.runDialogs), buttonView.isChecked());
                editor.apply();
            }
        });
        removeDialogs.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean(getString(R.string.runDialogs), buttonView.isChecked());
                editor.apply();
            }
        });
        db.closeDB();
    }

    public void displayHelper(boolean isChecked, final int id, String text){
        TextView textView = (TextView)myView.findViewById(id);
        if(isChecked){
            textView.setText(text + " are enabled");
        }
        else{
            textView.setText(text + " are disabled");
        }
    }

    public void save(){
        EditText userName = (EditText)myView.findViewById(R.id.username);
        db.updateMyName(userName.getText().toString());
        db.closeDB();
        View view = getActivity().getCurrentFocus();
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromInputMethod(view.getWindowToken(), 0);
    }

}
