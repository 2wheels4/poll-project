package com.example.andrew.pollproject;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.andrew.pollproject.com.example.andrew.model.DatabaseHelper;

import java.util.ArrayList;

/**
 * Created by Andrew on 6/28/2015.
 */
public class FindPeople extends Fragment {

    ArrayList<String> groupNames = new ArrayList<String>();
    DatabaseHelper db;
    View myView;
    Dialog dialog;
    EditText newGroup;
    Button confirm, cancel;
    String newGroupName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.find_people, container, false);
        return myView;
    }
}
